/**
 * Define jQuery plugins
 */
(function($) {
  /**
   * jQuery plugin to hide element without interfacing with drupal rules.
   */
  $.fn.wizardifyHide = function() {
    return this.each(function() {
      var $this = $(this);
      $this
        .addClass('wizardify-hide')
        .trigger('wizardify:hide');

      //Also trigger the event globally so it can be accessible everywhere.
      var settings = {
        'target' : $this
      }
      $("html").trigger("wizardify:global:hide", [settings]);
    });
  };

  /**
   * jQuery plugin to show element without interfacing with drupal rules.
   */
  $.fn.wizardifyUnHide = function() {
    return this.each(function() {
      var $this = $(this);
      $this.removeClass('wizardify-hide')
      .trigger('wizardify:unhide');

      //Also trigger the event globally so it can be accessible everywhere.
      var settings = {
        'target' : $this
      }
      $("html").trigger("wizardify:global:unhide", [settings]);
    });
  };

  /**
   * jQuery plugin to check if the element is hided by wizardify or not.
   * Note: This method is not chainable and does not return jQuery object.
   *
   * @returns bool True if item is hided by wizardify.
   */
  $.fn.wizardifyCheckHiddenStatus = function() {
    return $(this).hasClass('wizardify-hide');
  };

  /**
   * Toggle hidden status.
   *
   * @returns {*}
   */
  $.fn.wizardifyToggle = function() {
    var $this = $(this);

    $this.trigger('wizardify:toggle');
    //Also trigger the event globally so it can be accessible everywhere.
    var settings = {
      'target' : $this
    }
    $("html").trigger("wizardify:global:toggle", [settings]);

    if ($this.wizardifyCheckHiddenStatus()) {
      return $this.wizardifyUnHide();
    }
    else {
      return $this.wizardifyHide();
    }
  };
})(jQuery);
