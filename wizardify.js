/**
 * @fileOverview Main logic for converting a html form into a wizard.
 * @author "Ahmad Hejazee" <mngafa@gmail.com>
 */

(function($) {
  Drupal.behaviors.wizardify = {
    'attach': function() {
      var next_text = Drupal.t('Next');
      var step_pointer = 1;
      var next_button = $("<div id=\"next-button\">" + next_text + "</div>");

      /**
       * Wrap elements so that all items in each step are wrapped
       * in a single div.
       */
      var length = $(".wizardify-step").length;
      for (var i = 1; i <= length; i++) {
        $(".wizardify-step-" + i)
        .wrapAll('<div class="wizardify-wrapper-step wizardify-wrapper-step-' + i + '" />');
      }

      /**
       * Initialize form state
       */
      //Append the next button to the first element
      next_button.appendTo(".wizardify-wrapper-step-1");
      //Hide all items except the first one
      $(".wizardify-wrapper-step")
        .not(".wizardify-wrapper-step-1")
        .wizardifyHide();

      /**
       * Bind click on next button
       */
      next_button.click(function(e) {
        step_pointer++;
        next_button.appendTo(".wizardify-wrapper-step-" + step_pointer);

        //Hide next items and unhide previous items
        var all = $('.wizardify-wrapper-step');
        for (var j = 1; j <= all.length; j++) {
          if (j <= step_pointer) {
            $(".wizardify-wrapper-step-" + j).wizardifyUnHide();
          }
          else {
            $(".wizardify-wrapper-step-" + j).wizardifyHide();
          }
        }

        //focus on the first input on the next item
        $(".wizardify-wrapper-step-" + step_pointer)
          .find("input, textarea, select")
          .eq(0)
          .focus();
      });

      /**
       * Listen on "wizardify:global:showall" event.
       * This event when triggered, will show all items
       */
      $('html').bind('wizardify:global:showall', function() {
        $(".wizardify-wrapper-step").wizardifyUnHide();
      });
    },
    'detach': function() {
      //remove the next button
      $('#next-button').remove();

      /**
       * unwrap elements that were previously wrapped
       */
      var length = $(".wizardify-step").length;
      for (var i = 1; i <= length; i++) {
        var items = $(".wizardify-step-" + i);
        if (items.parent().is('.wizardify-wrapper-step')) {
          items.unwrap();
        }
      }

      //show all items
      $('html').trigger('wizardify:global:showall');

      /**
       * Unbind event listeners.
       */
      $('html').unbind('wizardify:global:showall');
    }
  };
})(jQuery);
